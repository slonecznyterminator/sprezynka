<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page session="false" %>
<html>
<head>
	<title>Add new user</title>
</head>
<body>
	<table>
		<tr>
			<td><label>First name:</label></td>
			<td><label>${user.firstName}</label></td>
		</tr>
		<tr>
			<td><label>Last name:</label></td>
			<td><label>${user.lastName}</label></td>
		</tr>
		<tr>
			<td><label>E-mail:</label></td>
			<td><label>${user.email}</label></td>
		</tr>
	</table>
	<spring:url value="/user/list" var="userList"/>
	<button onclick="location.href='${userList}'">Back</button>
</body>
</html>
