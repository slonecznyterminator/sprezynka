<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page session="false" %>
<html>
<head>
	<title>Add new user</title>
</head>
<body>
<form:form method="POST" action="" modelAttribute="user">
	<table>
		<tr>
			<td><label>First name:</label></td>
			<td><form:input type="text" path="firstName"></form:input></td>
		</tr>
		<tr>
			<td><label>Last name:</label></td>
			<td><form:input type="text" path="lastName"></form:input></td>
		</tr>
		<tr>
			<td><label>E-mail:</label></td>
			<td><form:input type="text" path="email"></form:input></td>
		</tr>
		<tr>
			<td><label>Password:</label></td>
			<td><form:input type="password" path="password"></form:input></td>
		</tr>
	</table>
	<button type="submit">Save</button>
	
</form:form>
	<spring:url value="/user/list" var="userList"/>
	<button onclick="location.href='${userList}'">Back</button>

</body>
</html>
