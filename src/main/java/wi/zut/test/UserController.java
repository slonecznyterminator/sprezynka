package wi.zut.test;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import wi.zut.test.model.User;

/**
 * Handles requests for the application home page.
 */
@Controller
@Transactional
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	private static final String SHOW_USER_LIST = "/user/list";
	private static final String SHOW_USER_DETAILS = "/user/details";
	private static final String SHOW_EXISTING_USER_DETAILS = "/user/{id}/details";
	private static final String SHOW_PREVIEW_USER_DETAILS = "/user/{id}/preview";
	private static final String DELETE_USER_DETAILS = "/user/{id}/delete";
	
	private static final String LIST_USER_VIEW = "user-list";
	private static final String EDITABLE_USER_VIEW = "user-details";
	private static final String PREVIEW_USER_VIEW = "user-preview";
	
	@Autowired
	private UserRepository repository;
	@Autowired
	private UserMail userMail;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Model model) {		
		return redirectToUserList();
	}
	
	private ModelAndView redirectToUserList() {
		return new ModelAndView("redirect:" + SHOW_USER_LIST);
	}
	
	@RequestMapping(value = SHOW_USER_LIST, method = RequestMethod.GET)
	public ModelAndView list() {
		List<User> allUsers = repository.findAll();
		
		ModelAndView model = new ModelAndView(LIST_USER_VIEW);
		model.addObject("allUsers", allUsers);
		return model;
	}
	
	@RequestMapping(value = SHOW_USER_DETAILS, method = RequestMethod.GET)
	public ModelAndView newUser() {
		ModelAndView modelAndView = new ModelAndView(EDITABLE_USER_VIEW);
		modelAndView.addObject("user", new User());
		
		return modelAndView;
	}
	
	@RequestMapping(value = SHOW_USER_DETAILS, method = RequestMethod.POST)
	public ModelAndView create(@ModelAttribute("user") User user) {
		logger.info("New user: {}", user);
		repository.save(user);
		userMail.userCreated(user);
		return redirectToUserList();
	}	
	
	@RequestMapping(value = SHOW_EXISTING_USER_DETAILS, method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id) {
		logger.info("Updating user with id: {}", id);
		Optional<User> maybeUser = Optional.ofNullable(repository.getOne(id));
		maybeUser.ifPresent(user -> Hibernate.initialize(user));
		
		ModelAndView modelAndView = maybeUser
				.map(user -> new ModelAndView(EDITABLE_USER_VIEW, Collections.singletonMap("user", user)))
				.orElseGet(() -> redirectToUserList());

		return modelAndView;
	}
	
	@RequestMapping(value = SHOW_EXISTING_USER_DETAILS, method = RequestMethod.POST)
	public ModelAndView edit(@PathVariable Long id, @ModelAttribute User user) {
		repository.save(user);
		return redirectToUserList();
	}
	
	@RequestMapping(value = SHOW_PREVIEW_USER_DETAILS, method = RequestMethod.GET)
	public ModelAndView preview(@PathVariable Long id) {
		logger.info("Preview of user with id: {}", id);
		return Optional.ofNullable(repository.getOne(id))
		.map(user -> new ModelAndView(PREVIEW_USER_VIEW, Collections.singletonMap("user", user)))
		.orElseGet(() -> redirectToUserList());
	}
	
	@RequestMapping(value = DELETE_USER_DETAILS, method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable Long id) {
		logger.info("Delete of user with id: {}", id);
		repository.delete(id);
		return redirectToUserList();
	}
}
