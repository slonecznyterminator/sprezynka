package wi.zut.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import wi.zut.test.model.User;

@Service
public class UserMail {

	private static final Logger logger = LoggerFactory.getLogger(UserMail.class);
	private final JavaMailSender mailSender;
	
	@Autowired
	public UserMail(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void userCreated(User user) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(user.getEmail());
		message.setSubject("User created");
		message.setText("User account has been created");
		
		try {
			mailSender.send(message);
		} catch(MailException ex) {
			logger.warn(ex.getMessage(), ex);
		}
	}
}
