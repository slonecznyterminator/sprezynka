package wi.zut.test.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import wi.zut.test.InMemoryRepository;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "wi.zut.test" })
@PropertySource("classpath:application.properties")
public class DataConfig {
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost:5432/prosta-baza-danych");
		dataSource.setUsername("postgres");
		dataSource.setPassword("prostehaslo");
		
		return dataSource;
	}
	
	@Bean
	public HibernateProperties hibernateProperties(Environment env) {
		return new HibernateProperties(env);
	}
	
	@Bean
	public EntityManagerFactory entityManagerFactory(@Autowired DataSource dataSource, @Autowired HibernateProperties properties) {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(dataSource);
		factory.setPackagesToScan("wi.zut.test");
		factory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		factory.setJpaProperties(properties.toProperties());
		factory.afterPropertiesSet();
		
		return factory.getObject();
	}
	
	@Bean
	public JpaTransactionManager transactionManager(@Autowired EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		
		return transactionManager;
	}
}
