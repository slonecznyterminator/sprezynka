package wi.zut.test;

import org.springframework.data.jpa.repository.JpaRepository;

import wi.zut.test.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
