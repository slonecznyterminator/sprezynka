package wi.zut.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import wi.zut.test.model.User;


//@Repository
public class InMemoryRepository {

	private static Long idGenerator = 0L;
	private final Map<Long, User> storage = new HashMap<>();
	
	public void save(User user) {
		user.setId(++idGenerator);
		storage.put(user.getId(), user);
	}
	
	public void update(User user) {
		storage.put(user.getId(), user);
	}
	
	public Optional<User> getOne(Long id) {
		return Optional.ofNullable(storage.get(id));
	}
	
	public void delete(Long id) {
		storage.remove(id);
	}
	
	public List<User> getAll() {
//		List<User> users = new ArrayList<>();
//		for(Map.Entry<Long, User> entry : storage.entrySet()) {
//			users.add(entry.getValue());
//		}
		
		return storage.entrySet()
				.stream()
				.map(entry -> entry.getValue())
				.collect(Collectors.toList());
	}
}
